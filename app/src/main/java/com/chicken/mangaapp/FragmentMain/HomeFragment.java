package com.chicken.mangaapp.FragmentMain;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chicken.mangaapp.Activity.DetailGenresActivity;
import com.chicken.mangaapp.Activity.DetailMangaActivity;
import com.chicken.mangaapp.Adapter.GenresAdapter;
import com.chicken.mangaapp.Adapter.LastUpdateAdapter;
import com.chicken.mangaapp.Adapter.NewMangaApdater;
import com.chicken.mangaapp.Adapter.TopTrendingAdapter;
import com.chicken.mangaapp.ItemClick;
import com.chicken.mangaapp.Model.Genres;
import com.chicken.mangaapp.Model.Toonily;
import com.chicken.mangaapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;



public class HomeFragment extends Fragment {
//    beta.blackdog.vip/api/listtopview
    private static HomeFragment instance;
    public static HomeFragment getInstance() {
        if (instance == null) {
            synchronized (HomeFragment.class) {
                instance = new HomeFragment();
            }
        }
        return instance;
    }
    public HomeFragment() {
        // Required empty public constructor
    }

    View viewRoot;
    RequestQueue requestQueue;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewRoot = getActivity().getLayoutInflater().inflate(R.layout.fragment_home, null, false);
        requestQueue = Volley.newRequestQueue(getContext());

        mappingGenres(viewRoot);
        mappingNewManga(viewRoot);
        mappingComplete(viewRoot);
        mappingTopTrending(viewRoot);
        mappingLastUpdate(viewRoot);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return viewRoot;
    }

    //genres
    RecyclerView rcv_genres;
    GenresAdapter genresAdapter;
    ArrayList<Genres> arrGenres;
    void mappingGenres(View view){
        arrGenres = new ArrayList<>();
        rcv_genres = view.findViewById(R.id.rcv_genres);
        genresAdapter = new GenresAdapter(getActivity(),arrGenres);
        genresAdapter.setItemClick(new ItemClick() {
            @Override
            public void onClickItem(int position) {
                Intent intent = new Intent(getActivity(), DetailGenresActivity.class);
                intent.putExtra("slug",arrGenres.get(position).getSlug());
                intent.putExtra("name",arrGenres.get(position).getName());
                startActivity(intent);
            }
        });
        rcv_genres.setAdapter(genresAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(RecyclerView.HORIZONTAL);
        rcv_genres.setLayoutManager(layoutManager);

        loadGenres();

    }
    void loadGenres(){
        String url = "https://beta.blackdog.vip/api/listcategorymanga";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0 ; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String name = jsonObject.getString("name");
                        String slug = jsonObject.getString("slug");
                        arrGenres.add(new Genres(name,slug));
                    }
                    genresAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);
    }

    void moveDetailActivity(String slug){
        Intent intent = new Intent(getActivity(), DetailMangaActivity.class);
        intent.putExtra("slug",slug);
        getActivity().startActivity(intent);
    }
    //new manga
    RecyclerView rcv_newmanga;
    ArrayList<Toonily> arrNewManga;
    NewMangaApdater newMangaApdater;
    void mappingNewManga(View view){
        arrNewManga = new ArrayList<>();
        rcv_newmanga = view.findViewById(R.id.rcv_newmanga);
        newMangaApdater = new NewMangaApdater(getActivity(),arrNewManga);
        newMangaApdater.setItemClick(new ItemClick() {
            @Override
            public void onClickItem(int position) {
                moveDetailActivity(arrNewManga.get(position).getComicSlug());
            }
        });
        rcv_newmanga.setAdapter(newMangaApdater);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getActivity());
        layoutManager2.setOrientation(RecyclerView.HORIZONTAL);
        rcv_newmanga.setLayoutManager(layoutManager2);

        loadNewmanga();
    }
    void loadNewmanga(){
        String url = "https://beta.blackdog.vip/api/listnewmanga";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0 ; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String comic_name = jsonObject.getString("comic_name");
                        String comic_slug = jsonObject.getString("comic_slug");
                        String comic_thumb = jsonObject.getString("comic_thumb");
                        arrNewManga.add(new Toonily(comic_name,comic_slug,comic_thumb,""));

                    }
                    newMangaApdater.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);
    }

    //list complete
    RecyclerView rcv_listcomplete;
    ArrayList<Toonily> arrComplete;
    NewMangaApdater completeApdater;
    void mappingComplete(View view){
        arrComplete = new ArrayList<>();
        rcv_listcomplete = view.findViewById(R.id.rcv_listcomplete);
        completeApdater = new NewMangaApdater(getActivity(),arrComplete);
        completeApdater.setItemClick(new ItemClick() {
            @Override
            public void onClickItem(int position) {
                moveDetailActivity(arrComplete.get(position).getComicSlug());
            }
        });
        rcv_listcomplete.setAdapter(completeApdater);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getActivity());
        layoutManager2.setOrientation(RecyclerView.HORIZONTAL);
        rcv_listcomplete.setLayoutManager(layoutManager2);

        loadComplete();
    }
    void loadComplete(){
        String url = "https://beta.blackdog.vip/api/listcompleted";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0 ; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String comic_name = jsonObject.getString("comic_name");
                        String comic_slug = jsonObject.getString("comic_slug");
                        String comic_thumb = jsonObject.getString("comic_thumb");
                        arrComplete.add(new Toonily(comic_name,comic_slug,comic_thumb,""));

                    }
                    completeApdater.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);
    }


    //top trending
    TopTrendingAdapter topTrendingAdapter;
    ArrayList<Toonily> arrTopTrending;
    RecyclerView rcv_TopTrending;

    void mappingTopTrending(View view){
        arrTopTrending = new ArrayList<>();
        rcv_TopTrending = view.findViewById(R.id.rcv_toptrending);
        topTrendingAdapter = new TopTrendingAdapter(getActivity(),arrTopTrending);
        topTrendingAdapter.setItemClick(new ItemClick() {
            @Override
            public void onClickItem(int position) {
                moveDetailActivity(arrTopTrending.get(position).getComicSlug());
            }
        });
        rcv_TopTrending.setAdapter(topTrendingAdapter);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),4);
        gridLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        rcv_TopTrending.setLayoutManager(gridLayoutManager);

        loadTrending();
    }
    void loadTrending(){
        String url = "https://beta.blackdog.vip/api/listtrending";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0 ; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String comic_name = jsonObject.getString("comic_name");
                        String comic_slug = jsonObject.getString("comic_slug");
                        String comic_thumb = jsonObject.getString("comic_thumb");
                        String created_at = jsonObject.getString("created_at");
                        arrTopTrending.add(new Toonily(comic_name,comic_slug,comic_thumb,created_at));
                    }
                    topTrendingAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);
    }


    //
    LastUpdateAdapter lastUpdateAdapter;
    ArrayList<Toonily> arrLastUpdate;
    RecyclerView rcv_LastUpdate;

    void mappingLastUpdate(View view){
        arrLastUpdate = new ArrayList<>();
        rcv_LastUpdate = view.findViewById(R.id.rcv_lastupdate);
        lastUpdateAdapter = new LastUpdateAdapter(getActivity(),arrLastUpdate);
        lastUpdateAdapter.setItemClick(new ItemClick() {
            @Override
            public void onClickItem(int position) {
                moveDetailActivity(arrLastUpdate.get(position).getComicSlug());
            }
        });
        rcv_LastUpdate.setAdapter(lastUpdateAdapter);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(),2);
        gridLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
        rcv_LastUpdate.setLayoutManager(gridLayoutManager);

        loadLastUpdate();
    }
    void loadLastUpdate(){
        String url = "https://beta.blackdog.vip/api/listlastestupdate";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0 ; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String comic_name = jsonObject.getString("comic_name");
                        String comic_slug = jsonObject.getString("comic_slug");
                        String comic_thumb = jsonObject.getString("comic_thumb");
                        String created_at = jsonObject.getString("created_at");
                        String comic_description = jsonObject.getString("comic_description");
                        String new_chapter = jsonObject.getString("new_chapter");
                        arrLastUpdate.add(new Toonily(comic_name,comic_slug,comic_thumb,created_at,comic_description,new_chapter));
                    }
                    lastUpdateAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);
    }
}