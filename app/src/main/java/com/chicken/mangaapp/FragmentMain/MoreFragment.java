package com.chicken.mangaapp.FragmentMain;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chicken.mangaapp.Activity.DetailMangaActivity;
import com.chicken.mangaapp.Adapter.NewMangaApdater;
import com.chicken.mangaapp.Database.TableFav;
import com.chicken.mangaapp.ItemClick;
import com.chicken.mangaapp.Model.Toonily;
import com.chicken.mangaapp.R;

import java.util.ArrayList;

public class MoreFragment extends Fragment {
    private static MoreFragment instance;
    public static MoreFragment getInstance() {
        if (instance == null) {
            synchronized (MoreFragment.class) {
                instance = new MoreFragment();
            }
        }
        return instance;
    }
    public MoreFragment() {
        // Required empty public constructor
    }

    View viewRoot;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewRoot = getActivity().getLayoutInflater().inflate(R.layout.fragment_more, null, false);
        tableFav = new TableFav(getActivity());
        mapping(viewRoot);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return viewRoot;
    }

    RecyclerView rcv_data_fav;
    ArrayList<Toonily> arrFav;
    NewMangaApdater newMangaApdater;
    TableFav tableFav;
    void mapping(View view){
        arrFav = new ArrayList<>();
        rcv_data_fav = view.findViewById(R.id.rcv_data_fav);
        newMangaApdater = new NewMangaApdater(getActivity(),arrFav);
        newMangaApdater.setItemClick(new ItemClick() {
            @Override
            public void onClickItem(int position) {
                Intent intent = new Intent(getActivity(), DetailMangaActivity.class);
                intent.putExtra("slug",arrFav.get(position).getComicSlug());
                getActivity().startActivity(intent);
            }
        });
        rcv_data_fav.setAdapter(newMangaApdater);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getActivity());
        layoutManager2.setOrientation(RecyclerView.HORIZONTAL);
        rcv_data_fav.setLayoutManager(layoutManager2);
    }

    @Override
    public void onResume() {
        super.onResume();
        arrFav.clear();
        if (tableFav.getListFav().size() != 0 ){
            arrFav.addAll(tableFav.getListFav());
            newMangaApdater.notifyDataSetChanged();
        }
    }
}