package com.chicken.mangaapp.FragmentMain;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chicken.mangaapp.Activity.DetailGenresActivity;
import com.chicken.mangaapp.Activity.SearchActivity;
import com.chicken.mangaapp.Adapter.GenresAdapter;
import com.chicken.mangaapp.ItemClick;
import com.chicken.mangaapp.Model.Genres;
import com.chicken.mangaapp.R;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SearchFragment extends Fragment {
    private static SearchFragment instance;
    public static SearchFragment getInstance() {
        if (instance == null) {
            synchronized (SearchFragment.class) {
                instance = new SearchFragment();
            }
        }
        return instance;
    }
    public SearchFragment() {
        // Required empty public constructor
    }

    View viewRoot;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewRoot = getActivity().getLayoutInflater().inflate(R.layout.fragment_search, null, false);
        mappingGenres(viewRoot);
        mappingSearch(viewRoot);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_home, container, false);
        return viewRoot;
    }

    //genres
    RecyclerView rcv_genres;
    GenresAdapter genresAdapter;
    ArrayList<Genres> arrGenres;
    RequestQueue requestQueue;
    void mappingGenres(View view){
        requestQueue = Volley.newRequestQueue(getContext());
        arrGenres = new ArrayList<>();
        rcv_genres = view.findViewById(R.id.rcv_genres);
        genresAdapter = new GenresAdapter(getActivity(),arrGenres,true);
        genresAdapter.setItemClick(new ItemClick() {
            @Override
            public void onClickItem(int position) {
                Intent intent = new Intent(getActivity(), DetailGenresActivity.class);
                intent.putExtra("slug",arrGenres.get(position).getSlug());
                intent.putExtra("name",arrGenres.get(position).getName());
                startActivity(intent);
            }
        });
        rcv_genres.setAdapter(genresAdapter);
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getContext());
        layoutManager.setFlexDirection(FlexDirection.ROW);
        layoutManager.setJustifyContent(JustifyContent.FLEX_START);
        rcv_genres.setLayoutManager(layoutManager);

        loadGenres();

    }
    void loadGenres(){
        String url = "https://beta.blackdog.vip/api/listcategorymanga";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0 ; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String name = jsonObject.getString("name");
                        String slug = jsonObject.getString("slug");
                        arrGenres.add(new Genres(name,slug));
                    }
                    genresAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);
    }


    //SEARCH
    SearchView searchView;
    void mappingSearch(View view){
        searchView = view.findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Intent intent = new Intent(getActivity(), SearchActivity.class);
                intent.putExtra("query",query.toString());
                startActivity(intent);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }

}