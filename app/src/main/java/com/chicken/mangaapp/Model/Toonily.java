package com.chicken.mangaapp.Model;

public class Toonily {

    private String comicName;
    private String comicSlug;
    private String comicThumb;
    private String createdAt;
    String comic_description;
    private  String new_chapter;

    public String getComic_description() {
        return comic_description;
    }

    public void setComic_description(String comic_description) {
        this.comic_description = comic_description;
    }

    public String getNew_chapter() {
        return new_chapter;
    }

    public void setNew_chapter(String new_chapter) {
        this.new_chapter = new_chapter;
    }

    public String getComicName() {
        return comicName;
    }

    public void setComicName(String comicName) {
        this.comicName = comicName;
    }

    public String getComicSlug() {
        return comicSlug;
    }

    public void setComicSlug(String comicSlug) {
        this.comicSlug = comicSlug;
    }

    public String getComicThumb() {
        return comicThumb;
    }

    public void setComicThumb(String comicThumb) {
        this.comicThumb = comicThumb;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public Toonily(String comicName, String comicSlug, String comicThumb, String createdAt) {
        this.comicName = comicName;
        this.comicSlug = comicSlug;
        this.comicThumb = comicThumb;
        this.createdAt = createdAt;
    }
    public Toonily(String comicName, String comicSlug, String comicThumb) {
        this.comicName = comicName;
        this.comicSlug = comicSlug;
        this.comicThumb = comicThumb;

    }


    public Toonily(String comicName, String comicSlug, String comicThumb, String createdAt, String comic_description, String new_chapter) {
        this.comicName = comicName;
        this.comicSlug = comicSlug;
        this.comicThumb = comicThumb;
        this.createdAt = createdAt;
        this.comic_description = comic_description;
        this.new_chapter = new_chapter;
    }
}
