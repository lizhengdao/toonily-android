package com.chicken.mangaapp.Model;

import org.json.JSONObject;

public class Chapter {
    private String chapterName;
    private String chapterSlug;
    private String comicSlug;
    private String createdAt;
    private int id;
    private int order;
    private String releaseDate;
    private String updatedAt;
    private String urlToonilyChapter;

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getChapterSlug() {
        return chapterSlug;
    }

    public void setChapterSlug(String chapterSlug) {
        this.chapterSlug = chapterSlug;
    }

    public String getComicSlug() {
        return comicSlug;
    }

    public void setComicSlug(String comicSlug) {
        this.comicSlug = comicSlug;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getUrlToonilyChapter() {
        return urlToonilyChapter;
    }

    public void setUrlToonilyChapter(String urlToonilyChapter) {
        this.urlToonilyChapter = urlToonilyChapter;
    }
    public Chapter(JSONObject jsonObject){
        if(jsonObject == null){
            return;
        }
        chapterName = jsonObject.optString("chapter_name");
        chapterSlug = jsonObject.optString("chapter_slug");
        comicSlug = jsonObject.optString("comic_slug");
        createdAt = jsonObject.optString("created_at");
        id = jsonObject.optInt("id");
        order = jsonObject.optInt("order");
        releaseDate = jsonObject.optString("release_date");
        updatedAt = jsonObject.optString("updated_at");
        urlToonilyChapter = jsonObject.optString("url_toonily_chapter");
    }

    public Chapter(String chapterName, String chapterSlug, String comicSlug, String createdAt, int id, int order, String releaseDate, String updatedAt, String urlToonilyChapter) {
        this.chapterName = chapterName;
        this.chapterSlug = chapterSlug;
        this.comicSlug = comicSlug;
        this.createdAt = createdAt;
        this.id = id;
        this.order = order;
        this.releaseDate = releaseDate;
        this.updatedAt = updatedAt;
        this.urlToonilyChapter = urlToonilyChapter;
    }
}
