package com.chicken.mangaapp.Activity;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.chicken.mangaapp.FragmentMain.HomeFragment;
import com.chicken.mangaapp.FragmentMain.MoreFragment;
import com.chicken.mangaapp.FragmentMain.SearchFragment;
import com.chicken.mangaapp.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView navigation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigation = findViewById(R.id.nav_view);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return loadFragment(item.getItemId());
            }
        });
        loadFragment(R.id.navigation_home);
    }

    public boolean loadFragment(int id) {
        Fragment fragment = null;
        String backStateName = "home";
        switch (id) {
            case R.id.navigation_home:
                fragment = new HomeFragment().getInstance();
                break;
            case R.id.navigation_search:
                fragment = new SearchFragment().getInstance();
                backStateName = "search";
                break;

            case R.id.navigation_more:
                fragment = new MoreFragment().getInstance();
                backStateName = "more";
                break;
        }

        replaceFragment(fragment,backStateName,id);
        return true;
    }
    public void replaceFragment (Fragment fragment, String backStateName, int i){
        FragmentManager fragmentManager = getSupportFragmentManager();
        if(fragmentManager.findFragmentByTag(backStateName) == null){
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.fragment_container, fragment);
            ft.addToBackStack(backStateName);
            ft.commit();
        }else{
            getSupportFragmentManager().popBackStack(backStateName,i);
        }
    }
}