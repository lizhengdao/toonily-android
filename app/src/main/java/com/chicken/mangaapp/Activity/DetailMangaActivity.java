package com.chicken.mangaapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chicken.mangaapp.Database.TableFav;
import com.chicken.mangaapp.Model.Toonily;
import com.chicken.mangaapp.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

public class DetailMangaActivity extends AppCompatActivity {
    ImageView image_back,image_fav,image_share,image_comic;
    TextView txt_title,txt_view,txt_titleBig,txt_Chapter,txt_star,txt_desc,txt_author,txt_Artist;
    RequestQueue requestQueue;
    FrameLayout frame_Readnow;
    String slug,referer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_manga);
        requestQueue = Volley.newRequestQueue(DetailMangaActivity.this);
        image_back = findViewById(R.id.image_back);
        image_fav = findViewById(R.id.image_fav);
        image_share = findViewById(R.id.image_share);

        image_comic = findViewById(R.id.image_comic);

        txt_title = findViewById(R.id.txt_title);
        txt_view = findViewById(R.id.txt_view);
        txt_titleBig = findViewById(R.id.txt_titleBig);
        txt_Chapter = findViewById(R.id.txt_Chapter);
        txt_star = findViewById(R.id.txt_star);
        txt_desc = findViewById(R.id.txt_desc);
        txt_author = findViewById(R.id.txt_author);
        txt_Artist = findViewById(R.id.txt_Artist);

        slug = getIntent().getStringExtra("slug");
        loadData(slug);

        frame_Readnow = findViewById(R.id.frame_Readnow);


        image_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        tableFav = new TableFav(DetailMangaActivity.this);
        if (tableFav.checkExist(slug) == true){
            image_fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_favorite_24));
        }else {
            image_fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_favorite_border_24));
        }
        image_fav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tableFav.checkExist(slug) == true){
                    tableFav.deleteFav(slug);
                    image_fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_favorite_border_24));
                }else {
                    tableFav.addToFav(new Toonily(NAME,SLUG,THUMB));
                    image_fav.setImageDrawable(getResources().getDrawable(R.drawable.ic_baseline_favorite_24));

                }
            }
        });
        image_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    TableFav tableFav;
    String SLUG,NAME,THUMB;
    void loadData(String slug){
        SLUG = slug;
        String url = "https://beta.blackdog.vip/api/manga_info/"+slug;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String name = jsonObject.getString("comic_name");
                    NAME = name;
                    String comic_thumb = jsonObject.getString("comic_thumb");
                    THUMB = comic_thumb;
                    String comic_author = jsonObject.getString("comic_author");
                    String comic_artist = jsonObject.getString("comic_artist");
                    String comic_rating = jsonObject.getString("comic_rating");
                    String new_chapter = jsonObject.getString("new_chapter");
                    String comic_description = jsonObject.getString("comic_description");
                    String comic_view = jsonObject.getString("comic_view");
                    referer = jsonObject.getString("toonily_url");
                    txt_star.setText(comic_rating);
                    txt_Artist.setText(comic_artist);
                    txt_author.setText(comic_author);
                    txt_desc.setText(comic_description);
                    txt_view.setText(comic_view);
                    txt_Chapter.setText(new_chapter);
                    txt_titleBig.setText(name);
                    txt_title.setText(name);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Picasso.get().load(comic_thumb).into(image_comic);

                            frame_Readnow.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(DetailMangaActivity.this, ListChapterActivity.class);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("slug",slug);
                                    bundle.putString("name",txt_title.getText().toString());
                                    bundle.putString("referer",referer);
                                    intent.putExtra("comic",bundle);
                                    startActivity(intent);
                                }
                            });
                        }
                    });


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);
    }
}