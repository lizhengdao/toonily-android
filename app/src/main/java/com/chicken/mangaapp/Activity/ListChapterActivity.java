package com.chicken.mangaapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chicken.mangaapp.Adapter.ChapterAdapter;
import com.chicken.mangaapp.ItemClick;
import com.chicken.mangaapp.Model.Chapter;
import com.chicken.mangaapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListChapterActivity extends AppCompatActivity {
    RecyclerView rcv_listchapter;
    ChapterAdapter chapterAdapter;
    ArrayList<Chapter> arrayList;
    TextView txt_title;
    String slug,referer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_chapter);
        slug = getIntent().getBundleExtra("comic").getString("slug");
        referer = getIntent().getBundleExtra("comic").getString("referer");
        String name = getIntent().getBundleExtra("comic").getString("name");

        txt_title = findViewById(R.id.txt_title);
        txt_title.setText(name);

        arrayList = new ArrayList<>();
        rcv_listchapter = findViewById(R.id.rcv_listchapter);
        chapterAdapter = new ChapterAdapter(ListChapterActivity.this,arrayList);
        chapterAdapter.setItemClick(new ItemClick() {
            @Override
            public void onClickItem(int position) {
                Chapter chapter = arrayList.get(position);
                Intent intent = new Intent(ListChapterActivity.this,ReadActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("slug",slug);
                bundle.putString("chapter_slug",chapter.getChapterSlug());
                bundle.putString("referer",referer);
                intent.putExtra("read",bundle);
                startActivity(intent);
            }
        });
        rcv_listchapter.setAdapter(chapterAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ListChapterActivity.this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        rcv_listchapter.setLayoutManager(linearLayoutManager);
        loadChapter(slug);
    }

    void loadChapter(String slug){
        RequestQueue requestQueue = Volley.newRequestQueue(ListChapterActivity.this);
        String url = "https://beta.blackdog.vip/api/manga_chapter/"+slug;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i<jsonArray.length() ; i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        arrayList.add(new Chapter(jsonObject));

                        chapterAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);
    }


}