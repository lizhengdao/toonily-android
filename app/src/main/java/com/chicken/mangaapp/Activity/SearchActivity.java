package com.chicken.mangaapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chicken.mangaapp.Adapter.ComicAdapter;
import com.chicken.mangaapp.ItemClick;
import com.chicken.mangaapp.Model.Toonily;
import com.chicken.mangaapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {
    SearchView searchView;
    RecyclerView rcv_data;
    ComicAdapter adapter;
    ArrayList<Toonily> arrayList;
    RequestQueue requestQueue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        requestQueue = Volley.newRequestQueue(SearchActivity.this);
        arrayList = new ArrayList<>();
        adapter = new ComicAdapter(SearchActivity.this,arrayList);
        adapter.setItemClick(new ItemClick() {
            @Override
            public void onClickItem(int position) {
                Intent intent = new Intent(SearchActivity.this, DetailMangaActivity.class);
                intent.putExtra("slug",arrayList.get(position).getComicSlug());
                startActivity(intent);
            }
        });

        mappingSearch();

        rcv_data = findViewById(R.id.rcv_data);
        rcv_data.setAdapter(adapter);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(SearchActivity.this,2);
        gridLayoutManager.setOrientation(RecyclerView.VERTICAL);
        rcv_data.setLayoutManager(gridLayoutManager);


        String query = getIntent().getStringExtra("query");
        Log.d("SearchActivity",query);
        searchView.setQuery(query,true);
        searchView.requestFocus();

        searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.requestFocusFromTouch();
        searchView.clearFocus();

    }

    void mappingSearch(){
        searchView = findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                loadData(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }
    void loadData(String query){
        arrayList.clear();
        String url = "https://beta.blackdog.vip/api/search/"+query;
        Log.d("SearchActivity",url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0 ; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String comic_name = jsonObject.getString("comic_name");
                        String comic_slug = jsonObject.getString("comic_slug");
                        String comic_thumb = jsonObject.getString("comic_thumb");
                        String created_at = jsonObject.getString("created_at");
                        arrayList.add(new Toonily(comic_name,comic_slug,comic_thumb,created_at));
                    }
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("SearchActivity",error.getMessage());
            }
        });
        requestQueue.add(stringRequest);
    }
}