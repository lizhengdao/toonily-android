package com.chicken.mangaapp.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chicken.mangaapp.Adapter.ComicAdapter;
import com.chicken.mangaapp.ItemClick;
import com.chicken.mangaapp.Model.Toonily;
import com.chicken.mangaapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DetailGenresActivity extends AppCompatActivity {
    TextView txt_title;

    RecyclerView rcv_data;
    ComicAdapter adapter;
    ArrayList<Toonily> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_genres2);
        txt_title = findViewById(R.id.txt_title);
        arrayList = new ArrayList<>();
        rcv_data = findViewById(R.id.rcv_data);
        adapter = new ComicAdapter(DetailGenresActivity.this,arrayList);
        adapter.setItemClick(new ItemClick() {
            @Override
            public void onClickItem(int position) {
                Intent intent = new Intent(DetailGenresActivity.this, DetailMangaActivity.class);
                intent.putExtra("slug",arrayList.get(position).getComicSlug());
                startActivity(intent);
            }
        });
        rcv_data.setAdapter(adapter);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(DetailGenresActivity.this,2);
        gridLayoutManager.setOrientation(RecyclerView.VERTICAL);
        rcv_data.setLayoutManager(gridLayoutManager);
        String title = getIntent().getStringExtra("name").toUpperCase();
        String slug = getIntent().getStringExtra("slug");
        txt_title.setText(title);

        loadData(slug);
    }
    RequestQueue requestQueue;
    void loadData(String slug){
        requestQueue = Volley.newRequestQueue(DetailGenresActivity.this);
        String url = "https://beta.blackdog.vip/api/listmangabycategory/"+slug;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0 ; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String comic_name = jsonObject.getString("comic_name");
                        String comic_slug = jsonObject.getString("comic_slug");
                        String comic_thumb = jsonObject.getString("comic_thumb");
                        String created_at = jsonObject.getString("created_at");
                        arrayList.add(new Toonily(comic_name,comic_slug,comic_thumb,created_at));
                    }
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("SearchActivity",error.getMessage());
            }
        });
        requestQueue.add(stringRequest);
    }
}