package com.chicken.mangaapp.Activity;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.chicken.mangaapp.Adapter.ItemImageChapterAdapter;
import com.chicken.mangaapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ReadActivity extends AppCompatActivity {
    ItemImageChapterAdapter adapter;
    ArrayList<String> arrayList;
    RecyclerView rcv_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);

        String slug = getIntent().getBundleExtra("read").getString("slug");
        String chapter_slug = getIntent().getBundleExtra("read").getString("chapter_slug");
        String referer = getIntent().getBundleExtra("read").getString("referer");

        rcv_data = findViewById(R.id.rcv_data);
        arrayList = new ArrayList<>();
         adapter  = new ItemImageChapterAdapter(ReadActivity.this,arrayList,referer);
        rcv_data.setAdapter(adapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ReadActivity.this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        rcv_data.setLayoutManager(linearLayoutManager);

        loadChapter(slug,chapter_slug);
    }
    void loadChapter(String slug,String chapter_slug){
        RequestQueue requestQueue = Volley.newRequestQueue(ReadActivity.this);
        String url = "http://beta.blackdog.vip/api/manga_detail_chapter/"+slug+"/"+chapter_slug;
        Log.d("loadChapter",url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i=0 ; i<jsonArray.length(); i ++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        arrayList.add(jsonObject.getString("img_src"));
                        Log.d("loadChapter",jsonObject.getString("img_src"));
                    }
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("loadChapter",error.getMessage());
            }
        });
        requestQueue.add(stringRequest);
    }
}