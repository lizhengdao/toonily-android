package com.chicken.mangaapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chicken.mangaapp.ItemClick;
import com.chicken.mangaapp.Model.Genres;
import com.chicken.mangaapp.R;

import java.util.ArrayList;

public class GenresAdapter extends RecyclerView.Adapter<GenresAdapter.ViewHoder> {
    ArrayList<Genres> arrayList;
    Context context;
    ItemClick itemClick;
    boolean isSearch = false;

    public void setItemClick(ItemClick itemClick) {
        this.itemClick = itemClick;
    }

    public GenresAdapter(Context context, ArrayList<Genres> arrayList ) {
        this.arrayList = arrayList;
        this.context = context;
    }
    public GenresAdapter(Context context, ArrayList<Genres> arrayList, boolean isSearch) {
        this.arrayList = arrayList;
        this.context = context;
        this.isSearch = isSearch;
    }

    @NonNull
    @Override
    public ViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (isSearch) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_genres_search, parent, false);
            return new ViewHoder(view);
        }else {
            View view = LayoutInflater.from(context).inflate(R.layout.item_genres, parent, false);
            return new ViewHoder(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHoder holder, int position) {
        holder.txt_genres.setText(arrayList.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    class ViewHoder extends RecyclerView.ViewHolder{
        TextView txt_genres;
        public ViewHoder(@NonNull View itemView) {
            super(itemView);
            txt_genres = itemView.findViewById(R.id.txt_genres);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClick.onClickItem(getAdapterPosition());
                }
            });
        }
    }
}
