package com.chicken.mangaapp.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chicken.mangaapp.ItemClick;
import com.chicken.mangaapp.Model.Toonily;
import com.chicken.mangaapp.R;
import com.chicken.mangaapp.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ComicAdapter extends RecyclerView.Adapter<ComicAdapter.ViewHoder> {
    Context context;
    ArrayList<Toonily> arrayList;
   ItemClick itemClick;

    public ComicAdapter(Context context, ArrayList<Toonily> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    public void setItemClick(ItemClick itemClick) {
        this.itemClick = itemClick;
    }

    @NonNull
    @Override
    public ViewHoder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_item_comic, parent, false);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        int width = (int) Utils.setItemRecyclerTopicEvent(context,2.2);
        layoutParams.width = width;
        layoutParams.height = (int)(width*1.5);

        view.setLayoutParams(layoutParams);

        return new ViewHoder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHoder holder, int position) {
        Log.d("SearchActivity",arrayList.get(position).getComicThumb());
        Picasso.get().load(arrayList.get(position).getComicThumb()).into(holder.image);
        holder.txt_title.setText(arrayList.get(position).getComicName());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    class ViewHoder extends RecyclerView.ViewHolder{
        ImageView image;
        TextView txt_title;
        public ViewHoder(@NonNull View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            txt_title = itemView.findViewById(R.id.txt_title);


        }
    }

}
