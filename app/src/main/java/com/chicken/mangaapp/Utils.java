package com.chicken.mangaapp;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;

public class Utils {
    public static float setItemRecyclerTopicEvent(Context context, double numberItem) {
        int size_item = 120;
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final int width = displayMetrics.widthPixels;
        final int height = displayMetrics.heightPixels;
        final int widthOfDp = (int) Utils.dpFromPx(context, width);
        final int heightOfDp = (int) Utils.dpFromPx(context, height);
        float item = (float) 0.0;
        size_item = heightOfDp;
        if (heightOfDp > widthOfDp) {
            size_item = widthOfDp;
        }
        double countRecy = numberItem;

        item = (float) ((double) widthOfDp / (double) (size_item / countRecy));
        return pxFromDp(context, widthOfDp / item);
    }
    public static float dpFromPx(final Context context, final float px) {
        try {
            return px / context.getResources().getDisplayMetrics().density;
        }catch (Exception e){
            return px;
        }
    }

    public static float pxFromDp(final Context context, final float dp) {
        try {
            return dp * context.getResources().getDisplayMetrics().density;
        }catch (Exception e){
            return dp;
        }
    }

}
