package com.chicken.mangaapp;

public interface ItemClick {
    void onClickItem(int position);
}
